# Feature Extractors in JS

After cloning the repo, run:

```bash
npm install
npm run build-example
```

Then open demo.html in a browser.

Feature Extractors in JavaScript can be implemented in a number of ways, but there are a few mandatory implementation details. 

The following inteface must be implemented:

```typescript
interface FeatureExtractor {
    configure(configuration: Configuration): ConfiguredOutputs;
    getDefaultConfiguration(): Configuration;
    process(block: ProcessInput): FeatureSet;
    finish(): FeatureSet;
}
```
As this is JavaScript, there are a number of different styles for implementing such an interface. This repo provides examples for implementing a Zero Crossings feature extractor using ES6 classes as well as a typical pattern for classes in ES5.

In addition, the Feature Extractor class should be exported as the default export, using ES6 style exports or the constructor function should be assigned to module.exports.default.

Each Feature Extractor is accompanied by a `feats-config.json` file. This defines the source file of the feature extractor, much like the `main` entry in a npm `package.json` file, as well as metadata describing what the feature extractor does, and basic details about the outputs it provides etc. 

Inspect the Zero Crossings examples for more concrete example.
