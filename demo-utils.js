function extractFeatures(pluginServer, pluginKey, audioBuffer, config) {
    var loadPlugin = function (pluginKey) {
        return pluginServer.loadPlugin({
            pluginKey: pluginKey,
            inputSampleRate: audioBuffer.sampleRate,
            adapterFlags: [Feats.AdapterFlags.AdaptAllSafe]
        });
    };

    var configure = function (response) {
        return pluginServer.configurePlugin({
            pluginHandle: response.pluginHandle,
            configuration: config
        });
    };

    var processBlocks = function (response) {
        var processThunk = function (block) {
            return pluginServer.process({pluginHandle: response.pluginHandle, processInput: block})
        };
        var finishThunk = function () {
            return pluginServer.finish({pluginHandle: response.pluginHandle});
        };
        return Feats.batchProcess(Feats.segmentAudioBuffer(config.blockSize, config.stepSize, audioBuffer),
                processThunk, finishThunk
        );
    };

    return loadPlugin(pluginKey)
            .then(configure)
            .then(processBlocks)
            .then(function (features) {
                console.log(features);
            })
            .catch(function (err) {
                console.log(err);
            });
}