const SampleType = require('feats').SampleType;
const frame2timestamp = require('feats').frame2timestamp;

function ZeroCrossings(inputSampleRate) {
    this.inputSampleRate = inputSampleRate;
    this.previousSample = 0;
}

ZeroCrossings.prototype.configure = function (configuration) {
    return new Map([
        ["counts", {
            binCount: 1,
            quantizeStep: 1.0,
            sampleType: SampleType.OneSamplePerStep,
            hasDuration: false,
            unit: "crossings"
        }],
        ["crossings", {
            binCount: 0,
            quantizeStep: 1,
            sampleType: SampleType.VariableSampleRate,
            sampleRate: this.inputSampleRate,
            hasDuration: false,
            unit: "",
        }]
    ])
}

ZeroCrossings.prototype.getDefaultConfiguration = function () {
    return {channelCount: 1, blockSize: 0, stepSize: 0};
}

ZeroCrossings.prototype.process = function (block) {
    let count = 0;
    let returnFeatures = new Map();
    let crossingPoints = [];

    const channel = block.inputBuffers[0]; // ignore stereo channels
    channel.forEach((sample, nSample) => {
        if (hasCrossedAxis(sample, this.previousSample)) {
            ++count;
            crossingPoints.push({timestamp: frame2timestamp(nSample, this.inputSampleRate)});
        }
        this.previousSample = sample;
    });

    returnFeatures.set("counts", [{featureValues: new Float32Array([count])}]);
    if (crossingPoints.length > 0) returnFeatures.set("crossings", crossingPoints);
    return returnFeatures;
}

ZeroCrossings.prototype.finish = function () {
    return new Map();
}

function hasCrossedAxis(sample, previousSample) {
    const hasCrossedFromAbove = previousSample > 0.0 && sample <= 0.0;
    const hasCrossedFromBelow = previousSample <= 0.0 && sample > 0.0;
    return hasCrossedFromBelow || hasCrossedFromAbove;
}

module.exports.default = ZeroCrossings;