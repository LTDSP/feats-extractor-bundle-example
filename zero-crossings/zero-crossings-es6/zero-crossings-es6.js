import {SampleType, frame2timestamp} from 'feats';

export default class ZeroCrossings {

	constructor(inputSampleRate) {
	    this.inputSampleRate = inputSampleRate;
	    this.previousSample = 0;	
	}
    
	configure (configuration) {
	    return new Map([
	        ["counts", {
	            binCount: 1,
	            quantizeStep: 1.0,
	            sampleType: SampleType.OneSamplePerStep,
	            hasDuration: false,
	            unit: "crossings"
	        }],
	        ["crossings", {
	            binCount: 0,
	            quantizeStep: 1,
	            sampleType: SampleType.VariableSampleRate,
	            sampleRate: this.inputSampleRate,
	            hasDuration: false,
	            unit: "",
	        }]
	    ])
	}

	getDefaultConfiguration () {
	    return {channelCount: 1, blockSize: 0, stepSize: 0};
	}

	process (block) {
	    let count = 0;
	    let returnFeatures = new Map();
	    let crossingPoints = [];

	    const channel = block.inputBuffers[0]; // ignore stereo channels
	    channel.forEach((sample, nSample) => {
	        if (this.hasCrossedAxis(sample)) {
	            ++count;
	            crossingPoints.push({timestamp: frame2timestamp(nSample, this.inputSampleRate)});
	        }
	        this.previousSample = sample;
	    });

	    returnFeatures.set("counts", [{featureValues: new Float32Array([count])}]);
	    if (crossingPoints.length > 0) returnFeatures.set("crossings", crossingPoints);
	    return returnFeatures;
	}

	finish () {
	    return new Map();
	}
	
	hasCrossedAxis(sample) {
	    const hasCrossedFromAbove = this.previousSample > 0.0 && sample <= 0.0;
	    const hasCrossedFromBelow = this.previousSample <= 0.0 && sample > 0.0;
	    return hasCrossedFromBelow || hasCrossedFromAbove;
	}
}
